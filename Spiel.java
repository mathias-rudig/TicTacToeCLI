package com.company;

import java.util.Scanner;

public class Spiel {
    //Datenfelder
    private int reihe;
    private int spalte;

    public Spiel(int reihe, int spalte) {
        setReihe(reihe);
        setSpalte(spalte);
    }

    public static void spielBeenden() {
        System.exit(0);
    }

    public int getSpalte() {
        return this.spalte;
    }

    public void setSpalte(int spalte) {
        this.spalte = spalte;
    }

    public int getReihe() {
        return this.reihe;
    }

    public void setReihe(int reihe) {
        this.reihe = reihe;
    }

    public void starteSpiel() {
        Scanner scan = new Scanner(System.in);
        System.out.println("********* Spiel beginnt **********");
        System.out.print("Bitte geben Sie den Namen für Spieler 1 ein: ");
        Spieler spieler1 = new Spieler(scan.nextLine(), 1, "X");
        System.out.print("Bitte geben Sie den Namen für Spieler 2 ein: ");
        Spieler spieler2 = new Spieler(scan.nextLine(), 2, "O");
        Spielfeld spielfeld = new Spielfeld(this.reihe, this.spalte);
        Spieler aktuellerSpieler = spieler1;
        spielfeld.spielfeldAusgeben(this.reihe, this.spalte);

        while (!spielfeld.spielUnentschieden() && !spielfeld.ueberpruefeSpielfeld(spieler1.getSpielerZeichen()) && !spielfeld.ueberpruefeSpielfeld(spieler2.getSpielerZeichen())) {
            int eingabeReihe;
            int eingabeSpalte;
            System.out.println(aktuellerSpieler.getSpielerName() + " Sie sind an der Reihe!");
            System.out.println("Bitte geben Sie ein freis Feld an");
            System.out.println("Reihe: ");
            eingabeReihe = pruefeEingabeReihe(scan.nextInt());
            System.out.println("Spalte: ");
            eingabeSpalte = pruefeEingabeSpalte(scan.nextInt());

            while (!spielfeld.setzeSpielstein(eingabeReihe, eingabeSpalte, aktuellerSpieler)) {
                System.out.println("Bitte geben Sie ein freies Feld an");
                System.out.println("Reihe: ");
                eingabeReihe = pruefeEingabeReihe(scan.nextInt());
                System.out.println("Spalte: ");
                eingabeSpalte = pruefeEingabeSpalte(scan.nextInt());
            }
            spielfeld.spielfeldAusgeben(this.reihe, this.spalte);
            if (!spielfeld.ueberpruefeSpielfeld(aktuellerSpieler.getSpielerZeichen()) && aktuellerSpieler.getSpielerNummer() == (spieler1.getSpielerNummer())) {
                aktuellerSpieler = spieler2;
            } else if (!spielfeld.ueberpruefeSpielfeld(aktuellerSpieler.getSpielerZeichen()) && aktuellerSpieler.getSpielerNummer() == (spieler2.getSpielerNummer())) {
                aktuellerSpieler = spieler1;
            }
        }
        System.out.println("Der Gewinner ist " + aktuellerSpieler.getSpielerName() + "!");
        spielBeenden();
    }

    public int pruefeEingabeReihe(int reihe) {
        int eingabe = reihe;
        Scanner scan = new Scanner(System.in);
        while (eingabe < 0 || eingabe > 3) {
            System.out.println("Eingabe nicht gültig, bitte versuchen Sie es noch einmal!");
            System.out.println("Reihe: ");
            eingabe = scan.nextInt();
        }
        eingabe -= 1;
        return eingabe;
    }

    public int pruefeEingabeSpalte(int spalte) {
        int eingabe = spalte;
        Scanner scan = new Scanner(System.in);
        while (eingabe < 0 || eingabe > 3) {
            System.out.println("Eingabe nicht gültig, bitte versuchen Sie es noch einmal!");
            System.out.println("Spalte: ");
            eingabe = scan.nextInt();
        }
        eingabe -= 1;
        return eingabe;
    }
}