package com.company;

public class Spieler {
    //Datenfelder
    private String spielerName;
    private int spielerNummer;
    private String spielerZeichen;

    public Spieler(String spielerName, int spielerNummer, String spielerZeichen) {
        setSpielerNummer(spielerNummer);
        setSpielerName(spielerName);
        setSpielerZeichen(spielerZeichen);
    }

    public String getSpielerName() {
        return this.spielerName;
    }

    public void setSpielerName(String spielerName) {
        if (spielerName.equals("")) {
            this.spielerName = "Spieler " + getSpielerNummer();
        } else {
            this.spielerName = spielerName;
        }
    }

    public int getSpielerNummer() {
        return this.spielerNummer;
    }

    public void setSpielerNummer(int spielerNummer) {
        this.spielerNummer = spielerNummer;
    }

    public String getSpielerZeichen() {
        return this.spielerZeichen;
    }

    public void setSpielerZeichen(String spielerZeichen) {
        this.spielerZeichen = spielerZeichen;
    }
}